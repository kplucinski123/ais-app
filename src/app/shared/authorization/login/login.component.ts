import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthorizationStep } from '../authorization.component';
import { AuthorizationService } from '../service/authorization.service';
import { FormUtilService } from '../../utils/form-util.service';
import { Token } from '../model/authorization.model';
import { AlertService } from '../../alert/service/alert.service';
import { LoginErrorComponent } from './error/login-error.component';
import { AlertType } from '../../alert/model/alert.model';

@Component({
    selector: 'ais-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.styl' ]
})
export class LoginComponent implements OnInit {
    @Input() public selectedStep: AuthorizationStep;
    @Output() public opened: EventEmitter<AuthorizationStep> = new EventEmitter<AuthorizationStep>();

    public step: AuthorizationStep = AuthorizationStep.LOGIN;
    public loginForm: FormGroup;

    constructor(
        private readonly authorizationService: AuthorizationService,
        private readonly router: Router,
        private readonly alertService: AlertService
    ) {}

    public ngOnInit(): void {
        this.loginForm = new FormGroup({
            email: new FormControl('', [ Validators.required ]),
            password: new FormControl('', [ Validators.required ])
        });
    }

    public checkStep(): boolean {
        return this.step === this.selectedStep;
    }

    public login(): void {
        if (this.loginForm.valid) {
            const email: string = this.loginForm.get('email').value;
            const password: string = this.loginForm.get('password').value;
            this.authorizationService.login({
                email,
                password
            }).subscribe({
                next: (response: Token) => this.success(response),
                error: ( error: Error ) => this.showAlert()
            });
        } else {
            FormUtilService.validateAllFormFields(this.loginForm);
        }
    }

    private success(response: Token): void {
        localStorage.setItem('aisUserToken', response.token);
        this.router.navigate([ '/' ]);
    }

    private showAlert(): void {
        this.alertService.show({
            component: LoginErrorComponent,
            type: AlertType.ERROR,
        });
    }
}
