import { Component } from '@angular/core';

@Component({
    selector: 'ais-login-error',
    templateUrl: './login-error.component.html'
})
export class LoginErrorComponent {}
