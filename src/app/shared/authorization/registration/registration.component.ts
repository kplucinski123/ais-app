import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthorizationStep } from '../authorization.component';
import { passwordStrength, userNameLength } from '../validator/registration.validator';
import { FormUtilService } from '../../utils/form-util.service';
import { AuthorizationService } from '../service/authorization.service';
import { AlertService } from '../../alert/service/alert.service';
import { AlertType } from '../../alert/model/alert.model';
import { SuccessRegistrationComponent } from './success/success-registration.component';

@Component({
    selector: 'ais-registration',
    templateUrl: './registration.component.html',
    styleUrls: [ './registration.component.styl' ]
})
export class RegistrationComponent implements OnInit {

    @Input() public selectedStep: AuthorizationStep;

    @Output() public opened: EventEmitter<AuthorizationStep> = new EventEmitter<AuthorizationStep>();

    public step: AuthorizationStep = AuthorizationStep.REGISTRATION;
    public registrationForm: FormGroup;

    constructor(
        private readonly authorizationService: AuthorizationService,
        private readonly alertService: AlertService
    ) {}

    public ngOnInit(): void {
        this.createRegistrationForm();
    }

    public checkStep(): boolean {
        return this.step === this.selectedStep;
    }

    public register(): void {
        if ( this.registrationForm.valid ) {
            const email: string = this.registrationForm.get('email').value;
            const userName: string = this.registrationForm.get('userName').value;
            const password: string = this.registrationForm.get('password').value;
            this.authorizationService.registration({email, userName, password}).subscribe({
                next: () => this.successRegistration(),
                error: ( error: Error ) => console.log(error)
            });
        } else {
            FormUtilService.validateAllFormFields(this.registrationForm);
        }
    }

    private successRegistration(): void {
        this.alertService.show({
            type: AlertType.SUCCESS,
            component: SuccessRegistrationComponent
        });
        this.createRegistrationForm();
        this.opened.emit(AuthorizationStep.LOGIN);
    }

    private createRegistrationForm(): void {
        this.registrationForm = new FormGroup({
            email: new FormControl('', [ Validators.required, Validators.email ]),
            userName: new FormControl('', [ Validators.required, userNameLength ]),
            password: new FormControl('', [ Validators.required, passwordStrength ])
        });
    }
}
