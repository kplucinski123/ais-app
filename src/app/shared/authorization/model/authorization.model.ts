export interface Registration {
    userName: string;
    password: string;
    email: string;
}

export interface User {
    email: string;
}

export interface Login {
    email: string;
    password: string;
}

export interface Token {
    token: string;
}
