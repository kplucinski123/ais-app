import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Login, Registration, Token, User } from '../model/authorization.model';
import { RestClientService } from '../../rest-client/rest-client.service';

@Injectable({
    providedIn: 'root'
})
export class AuthorizationService {
    constructor(
        private readonly restClientService: RestClientService
    ) {}

    public registration(dto: Registration): Observable<User> {
        return this.restClientService.post('/user', dto);
    }

    public login(dto: Login): Observable<Token> {
        return this.restClientService.post('/token', dto);
    }
}
