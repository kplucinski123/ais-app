import { Component } from '@angular/core';

@Component({
    selector: 'ais-authorization',
    templateUrl: './authorization.component.html',
    styleUrls: [ './authorization.component.styl' ]
})
export class AuthorizationComponent {
    public readonly stepTypes: typeof AuthorizationStep = AuthorizationStep;

    public selectedStep: AuthorizationStep = AuthorizationStep.REGISTRATION;

    public setStep(step: AuthorizationStep): void {
        this.selectedStep = step;
    }

}

export enum AuthorizationStep {
    LOGIN = 'login',
    REGISTRATION = 'registration'
}
