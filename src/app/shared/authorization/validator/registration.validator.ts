import { AbstractControl, ValidationErrors } from '@angular/forms';

export function userNameLength(control: AbstractControl): ValidationErrors {
    const userName: string = control.value;
    return userName.length < 5 ? {loginTooShort: true} : null;
}

export function passwordStrength(control: AbstractControl): ValidationErrors {
    const password: string = control.value;
    const hasNumber: boolean = /\d/.test(password);
    const hasUppercase: boolean = /[A-Z]/.test(password);
    const hasLowercase: boolean = /[a-z]/.test(password);
    const valid = hasNumber && hasUppercase && hasLowercase && password.length >= 8;
    return valid ? null : {passwordIsToWeak: true};
}
