import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/internal/operators';

import { environment } from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthorizeGuard implements CanActivate {
    private readonly tokenName = 'aisUserToken';
    private readonly api: string = environment.apiPath;

    constructor(
        private readonly http: HttpClient,
        private readonly router: Router
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const token: string = localStorage.getItem(this.tokenName);
        if (token) {
            return this.http.get<any>(this.api + '/token').pipe(
                take(1),
                map(() => true),
                catchError(() => this.logout())
            );
        }
        return this.logout();
    }

    private logout(): Observable<boolean> {
        localStorage.removeItem(this.tokenName);
        this.router.navigate([ '/authorization' ]);
        return of(false);
    }
}
