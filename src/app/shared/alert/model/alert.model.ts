import { Type } from '@angular/core';

export class AlertModel {
    type: AlertType;
    text?: string;
    component?: Type<any>;
}

export enum AlertType {
    SUCCESS = 'success',
    INFO = 'info',
    WARNING = 'warning',
    ERROR = 'error'
}
