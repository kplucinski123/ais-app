import {
    AfterViewInit,
    Component,
    ComponentFactory,
    ComponentFactoryResolver,
    ComponentRef,
    OnDestroy,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import { animate, AnimationEvent, state, style, transition, trigger } from '@angular/animations';
import { OverlayRef } from '@angular/cdk/overlay';

import { AlertModel, AlertType } from '../model/alert.model';

export type AlertAnimationState = 'default' | 'closing';

@Component({
    selector: 'ais-alert',
    templateUrl: './alert.component.html',
    styleUrls: [ 'alert.component.styl' ],
    animations: [ trigger('fadeAnimation', [
        state('default', style({opacity: 1})),
        transition('void => *', [ style({opacity: 0}), animate('{{ fadeIn }}ms') ]),
        transition(
            'default => closing',
            animate('{{ fadeOut }}ms', style({opacity: 0})),
        )
    ]) ],
})
export class AlertComponent implements OnInit, OnDestroy, AfterViewInit {
    public animationState: AlertAnimationState = 'default';
    public iconType: string;
    public alertText: string;

    private intervalId: number;

    @ViewChild('dynamic', {read: ViewContainerRef, static: true}) private viewContainerRef: ViewContainerRef;

    constructor(
        private readonly alertModel: AlertModel,
        private readonly componentFactoryResolver: ComponentFactoryResolver,
        private readonly overlayRef: OverlayRef
    ) {}

    public ngOnInit(): void {
        this.alertText = this.alertModel.text;
        this.iconType = this.alertModel.type === 'success' ? 'done' : this.alertModel.type;
        this.intervalId = window.setTimeout(() => this.animationState = 'closing', 5000);
    }

    public ngAfterViewInit(): void {
        if (!this.alertModel.text && this.alertModel.component) {
            const componentFactory: ComponentFactory<any> = this.componentFactoryResolver.resolveComponentFactory(this.alertModel.component);
            const componentRef: ComponentRef<any> = this.viewContainerRef.createComponent(componentFactory);
            componentRef.changeDetectorRef.detectChanges();
        }
    }

    public ngOnDestroy(): void {
        window.clearTimeout(this.intervalId);
    }

    public close(): void {
        this.overlayRef.dispose();
    }

    public onFadeFinished(event: AnimationEvent): void {
        const isFadeOut: boolean = event.toState as AlertAnimationState === 'closing';
        const itFinished: boolean = this.animationState === 'closing';
        if (isFadeOut && itFinished) {
            this.close();
        }
    }

    public getClass(): string {
        switch (this.alertModel.type) {
            case AlertType.SUCCESS:
                return 'alert success';
            case AlertType.WARNING:
                return 'alert warning';
            case AlertType.INFO:
                return 'alert info';
            case AlertType.ERROR:
                return 'alert error';
            default:
                return 'alert';
        }
    }
}
