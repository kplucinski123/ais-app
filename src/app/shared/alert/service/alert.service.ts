import { Injectable, Injector } from '@angular/core';
import { GlobalPositionStrategy, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';

import { AlertComponent } from '../component/alert.component';
import { AlertModel } from '../model/alert.model';

@Injectable({
    providedIn: 'root'
})
export class AlertService {
    private lastAlert: OverlayRef;

    constructor(
        private readonly overlay: Overlay,
        private readonly parentInjector: Injector
    ) {}

    public show(alertModel: AlertModel): OverlayRef {
        const overlayRef: OverlayRef = this.overlay.create({positionStrategy: this.getPositionStrategy()});
        this.lastAlert = overlayRef;
        const injector: PortalInjector = this.getInjector(alertModel, overlayRef);
        const alertPortal: ComponentPortal<any> = new ComponentPortal(AlertComponent, null, injector);
        overlayRef.attach(alertPortal);
        return overlayRef;
    }

    private getPositionStrategy(): GlobalPositionStrategy {
        return this.overlay.position()
            .global()
            .centerHorizontally()
            .top(this.getPosition());
    }

    private getPosition(): string {
        const lastAlertIsVisible = this.lastAlert && this.lastAlert.overlayElement;
        const position = lastAlertIsVisible
            ? this.lastAlert.overlayElement.getBoundingClientRect().bottom
            : 30;
        return `${ position }px`;
    }

    private getInjector(data: AlertModel, alertRef: OverlayRef): PortalInjector {
        const tokens = new WeakMap();
        tokens.set(AlertModel, data);
        tokens.set(OverlayRef, alertRef);
        return new PortalInjector(this.parentInjector, tokens);
    }
}
