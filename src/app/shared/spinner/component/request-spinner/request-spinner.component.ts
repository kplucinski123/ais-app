import { Component, OnInit } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { RequestSpinnerService } from '../../service/request-spinner.service';

@Component({
    selector: 'ais-request-spinner',
    templateUrl: 'request-spinner.component.html',
    styleUrls: [ 'request-spinner.component.styl' ]
})
export class RequestSpinnerComponent implements OnInit {

    public loading: BehaviorSubject<boolean>;

    constructor(
        private readonly requestSpinnerService: RequestSpinnerService
    ) {}

    public ngOnInit(): void {
        this.loading = this.requestSpinnerService.loading;
    }
}
