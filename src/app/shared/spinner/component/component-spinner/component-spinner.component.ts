import { Component } from '@angular/core';

@Component({
    selector: 'ais-component-spinner',
    templateUrl: 'component-spinner.component.html',
    styleUrls: [ 'component-spinner.component.styl' ]
})
export class ComponentSpinnerComponent {}
