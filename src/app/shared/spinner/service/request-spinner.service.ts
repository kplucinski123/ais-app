import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class RequestSpinnerService {

    public loading: BehaviorSubject<boolean> = new BehaviorSubject(false);

    private requests: string[] = [];

    public start(url: string): void {
        this.requests.push(url);
        this.loading.next(true);
    }

    public stop(url: string): void {
        setTimeout(() => {
            if (this.requests.length === 0) {
                return;
            }
            const indexToRemove = this.requests.indexOf(url, 0);
            if (indexToRemove > -1) {
                this.requests.splice(indexToRemove, 1);
            }
            if (this.requests.length === 0) {
                this.loading.next(false);
            }
        }, 500);
    }
}
