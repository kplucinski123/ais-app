import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { finalize, take } from 'rxjs/internal/operators';

import { environment } from '../../../environments/environment';
import { RequestSpinnerService } from '../spinner/service/request-spinner.service';

@Injectable({
    providedIn: 'root'
})
export class RestClientService {

    private readonly apiPath: string = environment.apiPath;

    constructor(
        private readonly httpClient: HttpClient,
        private readonly requestSpinnerService: RequestSpinnerService
    ) {}

    public post<T, R>(url: string, body: T, spinner: boolean = false): Observable<R> {
        const spinnerKey = `post:${ url }`;
        this.startSpinner(spinnerKey, spinner);
        return this.httpClient.post<R>(this.apiPath + url, body, {observe: 'body'}).pipe(
            take(1),
            finalize(() => this.stopSpinner(spinnerKey, spinner))
        );
    }

    public delete<T, R>( url: string, body?: T, spinner: boolean = false ): Observable<R> {
        const spinnerKey = `delete:${ url }`;
        this.startSpinner(spinnerKey, spinner);
        return this.httpClient.delete<R>(this.apiPath + url).pipe(
            take(1),
            finalize(() => this.stopSpinner(spinnerKey, spinner))
        );
    }

    public get<R>(url: string, spinner: boolean = false): Observable<R> {
        const spinnerKey = `get:${ url }`;
        this.startSpinner(spinnerKey, spinner);
        return this.httpClient.get<R>(this.apiPath + url, {observe: 'body'}).pipe(
            take(1),
            finalize(() => this.stopSpinner(spinnerKey, spinner))
        );
    }

    private startSpinner(key: string, spinner: boolean): void {
        if (spinner) {
            this.requestSpinnerService.start(key);
        }
    }

    private stopSpinner(key: string, spinner: boolean): void {
        if (spinner) {
            this.requestSpinnerService.stop(key);
        }
    }
}
