import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { WindowService } from './service/window.service';

@Component({
  selector: 'ais-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl']
})
export class AppComponent implements OnInit {
  constructor(
      private readonly breakpointObserver: BreakpointObserver,
      private readonly windowService: WindowService
  ) {}

  public ngOnInit(): void {
    this.breakpointObserver.observe([ '(min-width: 720px)' ])
        .subscribe(( state: BreakpointState ) => this.windowService.smallScreen = !state.matches);
  }
}
