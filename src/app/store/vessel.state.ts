import { Injectable } from '@angular/core';
import { Sort } from '@angular/material/sort';

import { Action, Selector, State, StateContext } from '@ngxs/store';

import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { map } from 'rxjs/internal/operators';

import { FilterVessel, VesselCoordinates, VesselDetailsModel, VesselModel, VesselsModel } from '../model/vessel.model';
import { VesselService } from '../service/vessel.service';
import {
    AddFavoriteVessel,
    ApplyFilterVessels,
    FetchCoordinates,
    InitFavoriteVessels,
    InitVessels,
    RemoveFavoriteVessel,
    SelectVessels
} from './vessel.action';

export interface VesselStateModel {
    all: VesselModel[];
    length: number;
    filtered: VesselModel[];
    selected: VesselDetailsModel;
    favorites: VesselModel[];
    coordinates: VesselCoordinates;
    filter: FilterVessel;
    loaded: boolean;
}

@State<VesselStateModel>({
    name: 'vessels',
    defaults: {
        all: [],
        favorites: [],
        filtered: [],
        selected: undefined,
        coordinates: undefined,
        length: undefined,
        filter: undefined,
        loaded: true
    }
})
@Injectable({
    providedIn: 'root'
})
export class VesselState {
    constructor(
        private readonly vesselService: VesselService
    ) {}

    @Selector()
    public static getFiltered(state: VesselStateModel): VesselModel[] {
        return state.filtered;
    }

    @Selector()
    public static getSelected(state: VesselStateModel): VesselDetailsModel {
        return state.selected;
    }

    @Selector()
    public static getCoordinates(state: VesselStateModel): VesselCoordinates {
        return state.coordinates;
    }

    @Selector()
    public static getLength(state: VesselStateModel): number {
        return state.length;
    }

    @Selector()
    public static getFavorites(state: VesselStateModel): VesselModel[] {
        return state.favorites;
    }

    @Selector()
    public static getLoaded(state: VesselStateModel): boolean {
        return state.loaded;
    }

    @Action(SelectVessels)
    public select(context: StateContext<VesselStateModel>, action: SelectVessels): void {
        this.vesselService.getDetails(action.id).subscribe(
            (details: VesselDetailsModel) => context.patchState({selected: details}),
            () => this.findDetailsFromAll(context, action.id)
        );
    }

    @Action(FetchCoordinates)
    public fetchCoordinates(context: StateContext<VesselStateModel>, action: FetchCoordinates): void {
        this.vesselService.getCoordinates(action.id)
            .subscribe((coordinates: VesselCoordinates) => context.patchState({coordinates}));
    }

    @Action(InitVessels)
    public init(context: StateContext<VesselStateModel>, action: InitVessels): void {
        context.patchState({loaded: false});
        this.vesselService.getVessels().subscribe((response: VesselsModel) =>
            context.patchState({
                all: response.vessels,
                filtered: this.filter(context, response.vessels, action.filter),
                length: response.vessels.length,
                filter: action.filter,
                favorites: response.vessels.filter((vessel: VesselModel) => vessel.favorite),
                loaded: true
            })
        );
    }

    @Action(InitFavoriteVessels)
    public initFavorite(context: StateContext<VesselStateModel>): Observable<void> {
        if (!context.getState().all.length) {
            context.patchState({loaded: false});
            return this.vesselService.getVessels().pipe(
                tap((response: VesselsModel) =>
                    context.patchState({
                        all: response.vessels,
                        length: 0,
                        favorites: response.vessels.filter((vessel: VesselModel) => vessel.favorite),
                        loaded: true
                    })),
                map(() => void 0)
            );
        }
        return of(void 0);
    }

    @Action(ApplyFilterVessels)
    public applyFilter(context: StateContext<VesselStateModel>, action: ApplyFilterVessels): void {
        context.patchState({loaded: false});
        const filter = context.getState().filter;
        if (action.filter.mmsi !== undefined) {
            filter.mmsi = action.filter.mmsi;
        }
        if (action.filter.name !== undefined) {
            filter.name = action.filter.name;
        }
        if (action.filter.pageSize !== undefined) {
            filter.pageSize = action.filter.pageSize;
        }
        if (action.filter.pageIndex !== undefined) {
            filter.pageIndex = action.filter.pageIndex;
        }
        if (action.filter.sort !== undefined) {
            filter.sort = action.filter.sort;
        }
        context.patchState({
            filtered: this.filter(context, context.getState().all, filter),
            loaded: true,
            filter
        });
    }

    @Action(AddFavoriteVessel)
    public addFavorite(context: StateContext<VesselStateModel>, action: AddFavoriteVessel): void {
        this.vesselService.addToFavorite(action.id).subscribe(() => {
            const all: VesselModel[] = context.getState().all;
            const favoriteVessel: VesselModel = all.find((vessel: VesselModel) => vessel.id === action.id);
            favoriteVessel.favorite = true;
            const index = all.indexOf(favoriteVessel);
            all[index] = favoriteVessel;
            const favorites: VesselModel[] = context.getState().favorites;
            favorites.push(favoriteVessel);
            context.patchState({favorites, all});
        });
    }

    @Action(RemoveFavoriteVessel)
    public removeFavorite(context: StateContext<VesselStateModel>, action: RemoveFavoriteVessel): void {
        this.vesselService.removeFromFavorite(action.id).subscribe(() => {
            const all: VesselModel[] = context.getState().all;
            const favoriteVessel: VesselModel = all.find((vessel: VesselModel) => vessel.id === action.id);
            favoriteVessel.favorite = false;
            const index = all.indexOf(favoriteVessel);
            all[index] = favoriteVessel;
            const favorites: VesselModel[] = context.getState().favorites.filter((vessel: VesselModel) => vessel.id !== action.id);
            context.patchState({favorites, all});
        });
    }

    private findDetailsFromAll(context: StateContext<VesselStateModel>, id: number): void {
        context.dispatch(new InitFavoriteVessels()).subscribe(() => {
            const selected: VesselModel = context.getState().all.find((vessel: VesselModel) => vessel.id === id);
            context.patchState({
                selected: {
                    mmsi: selected.mmsi,
                    name: selected.name,
                    country: selected.country,
                    shipType: '' + selected.shipType
                } as VesselDetailsModel
            });
        });
    }

    private filter(context: StateContext<VesselStateModel>, vessels: VesselModel[], filter: FilterVessel): VesselModel[] {
        const sorted: VesselModel[] = vessels
            .filter((vessel: VesselModel) => !filter.name || (vessel.name && vessel.name.toUpperCase().includes(filter.name.toUpperCase())))
            .filter((vessel: VesselModel) => !filter.mmsi || (vessel.mmsi && vessel.mmsi.toUpperCase().includes(filter.mmsi.toUpperCase())))
            .sort((first: VesselModel, second: VesselModel) => this.sort(first, second, filter.sort));
        context.patchState({length: sorted.length});
        return sorted.slice(filter.pageIndex * filter.pageSize, (filter.pageIndex + 1) * filter.pageSize);
    }

    private sort(first: VesselModel, second: VesselModel, sort: Sort): number {
        switch (sort.active) {
            case 'name':
                return this.compare(sort.direction, first.name, second.name);
            case 'mmsi':
                return this.compare(sort.direction, first.mmsi, second.mmsi);
            case 'country':
                return this.compare(sort.direction, first.country, second.country);

        }
        return 0;
    }

    private compare(direction: string, first: string, second: string): number {
        if (direction === 'desc') {
            if (!first) {
                return 1;
            }
            if (!second) {
                return -1;
            }
            return second.localeCompare(first);
        } else {
            if (!first) {
                return -1;
            }
            if (!second) {
                return 1;
            }
            return first.localeCompare(second);
        }
    }
}
