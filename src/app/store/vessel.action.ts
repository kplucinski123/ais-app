import { FilterVessel } from '../model/vessel.model';

export class InitVessels {
    public static readonly type = '[Vessel] Init';

    constructor(public readonly filter: FilterVessel) {}
}

export class SelectVessels {
    public static readonly type = '[Vessel] Select';

    constructor(public readonly id: number) {}
}

export class FetchCoordinates {
    public static readonly type = '[Vessel] Fetch Coordinates';

    constructor(public readonly id: number) {}
}

export class InitFavoriteVessels {
    public static readonly type = '[Vessel] Init Favorite';
}

export class ApplyFilterVessels {
    public static readonly type = '[Vessel] Apply Filter';

    constructor(public readonly filter: FilterVessel) {}
}

export class AddFavoriteVessel {
    public static readonly type = '[Vessel] Add Favorite';

    constructor(public readonly id: number) {}
}

export class RemoveFavoriteVessel {
    public static readonly type = '[Vessel] Remove Favorite';

    constructor(public readonly id: number) {}
}
