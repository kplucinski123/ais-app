import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { RestClientService } from '../shared/rest-client/rest-client.service';
import { VesselCoordinates, VesselDetailsModel, VesselsModel } from '../model/vessel.model';

@Injectable({
    providedIn: 'root'
})
export class VesselService {
    constructor(
        private readonly restClientService: RestClientService
    ) {}

    public getVessels(): Observable<VesselsModel> {
        return this.restClientService.get('/vessels');
    }

    public getCoordinates(id: number): Observable<VesselCoordinates> {
        return this.restClientService.get(`/vessels/${id}/coordinates`);
    }

    public getDetails(id: number): Observable<VesselDetailsModel> {
        return this.restClientService.get(`/vessels/${id}`);
    }

    public addToFavorite(id: number): Observable<void> {
        return this.restClientService.post(`/vessels/${id}/favorite`, {});
    }

    public removeFromFavorite(id: number): Observable<void> {
        return this.restClientService.delete(`/vessels/${id}/favorite`);
    }
}
