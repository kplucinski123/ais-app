import { Injectable } from '@angular/core';
import { RestClientService } from '../shared/rest-client/rest-client.service';
import { Observable } from 'rxjs';
import { VesselNotification } from '../model/vessel.model';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    constructor(
        private readonly restClientService: RestClientService
    ) {}

    public registerNotificationTask(vesselId: number, notification: VesselNotification): Observable<void> {
        return this.restClientService.post(`/notifications/vessels/${vesselId}/tasks`, notification);
    }
}
