import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Overlay } from '@angular/cdk/overlay';

import { NgxsModule } from '@ngxs/store';

import { AppComponent } from './app.component';
import { AuthorizationComponent } from './shared/authorization/authorization.component';
import { MaterialModule } from './shared/material/material.module';
import { RegistrationComponent } from './shared/authorization/registration/registration.component';
import { LoginComponent } from './shared/authorization/login/login.component';
import { AuthorizeGuard } from './shared/authorization/guards/authorize.guard';
import { TokenInterceptor } from './shared/authorization/interceptors/token.interceptor';
import { AlertComponent } from './shared/alert/component/alert.component';
import { SuccessRegistrationComponent } from './shared/authorization/registration/success/success-registration.component';
import { LoginErrorComponent } from './shared/authorization/login/error/login-error.component';
import { TopBarComponent } from './component/top-bar/top-bar.component';
import { LeftMenuLayoutComponent } from './component/left-menu-layout/left-menu-layout.component';
import { MainComponent } from './component/main/main.component';
import { ComponentSpinnerComponent } from './shared/spinner/component/component-spinner/component-spinner.component';
import { RequestSpinnerComponent } from './shared/spinner/component/request-spinner/request-spinner.component';
import { VesselState } from './store/vessel.state';
import { VesselComponent } from './component/vessel/vessel.component';
import { MapsComponent } from './component/maps/maps.component';
import { RegisterTaskComponent } from './component/vessel/registertask/register-task.component';

const routes: Routes = [
    {path: 'authorization', component: AuthorizationComponent},
    {path: 'main', component: MainComponent, canActivate: [ AuthorizeGuard ]},
    {path: 'vessel/:id', component: VesselComponent, canActivate: [ AuthorizeGuard ]},
    {path: '**', redirectTo: 'main'}
];

@NgModule({
    declarations: [
        AppComponent,
        RegistrationComponent,
        LoginComponent,
        AuthorizationComponent,
        AlertComponent,
        LoginErrorComponent,
        SuccessRegistrationComponent,
        TopBarComponent,
        LeftMenuLayoutComponent,
        MainComponent,
        ComponentSpinnerComponent,
        RequestSpinnerComponent,
        VesselComponent,
        MapsComponent,
        RegisterTaskComponent
    ],
    imports: [
        NgxsModule.forRoot([
            VesselState
        ]),
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        BrowserModule,
        RouterModule.forRoot(routes),
        BrowserAnimationsModule,
        HttpClientModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        Overlay
    ],
    entryComponents: [
        AlertComponent,
        MapsComponent,
        RegisterTaskComponent
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {}
