import { Sort } from '@angular/material/sort';

export interface VesselModel {
    id: number;
    mmsi: string;
    shipType: number;
    country: string;
    name: string;
    favorite: boolean;
}

export interface VesselsModel {
    vessels: VesselModel[];
}

export interface VesselCoordinates {
    longitude: number;
    latitude: number;
}

export interface VesselNotification {
    maxLatitude: number;
    minLatitude: number;
    maxLongitude: number;
    minLongitude: number;
    deadLineInMinutes: number;
}

export interface VesselDetailsModel {
    mmsi: string;
    name: string;
    imo: string;
    ircs: string;
    country: string;
    shipType: string;
    cog: string;
    shipRegisterName: string;
    aisName: string;
}

export class FilterVessel {
    name?: string;
    mmsi?: string;
    pageIndex?: number;
    pageSize?: number;
    sort?: Sort;
}
