import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Loader, LoaderOptions } from '@googlemaps/js-api-loader';

import { Store } from '@ngxs/store';
import { interval, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/internal/operators';

import { VesselCoordinates, VesselDetailsModel, VesselNotification } from '../../model/vessel.model';
import { FetchCoordinates, SelectVessels } from '../../store/vessel.action';
import { VesselState } from '../../store/vessel.state';
import { environment } from '../../../environments/environment';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { MapsComponent } from '../maps/maps.component';
import { NotificationService } from '../../service/notification.service';
import { flatMap } from 'rxjs/operators';
import { AlertService } from '../../shared/alert/service/alert.service';
import { AlertType } from '../../shared/alert/model/alert.model';
import { RegisterTaskComponent } from './registertask/register-task.component';

@Component({
    selector: 'ais-vessel',
    templateUrl: 'vessel.component.html',
    styleUrls: ['vessel.component.styl']
})
export class VesselComponent implements OnInit, OnDestroy {
    public vessel: VesselDetailsModel;

    @ViewChild('mapTest') private mapElement: ElementRef;

    private unsubscribe$: Subject<void> = new Subject();
    private id: number;
    private coordinates: VesselCoordinates;

    constructor(
        private readonly store: Store,
        private readonly route: ActivatedRoute,
        private readonly dialog: MatDialog,
        private readonly notificationService: NotificationService,
        private readonly alertService: AlertService
    ) {}

    public ngOnInit(): void {
        this.store.select(VesselState.getSelected).subscribe((vessel: VesselDetailsModel) => this.vessel = vessel);
        this.store.select(VesselState.getCoordinates).subscribe((coordinates: VesselCoordinates) => {
            this.coordinates = coordinates;
            const options: LoaderOptions = {apiKey: environment.mapApiKey};
            new Loader(options).load().then(() => this.initMap(coordinates));
        });
        this.route.params.pipe(
            map(params => +params.id),
            takeUntil(this.unsubscribe$)
        ).subscribe((id: number) => {
            this.id = id;
            this.store.dispatch(new SelectVessels(id));
            this.store.dispatch(new FetchCoordinates(id));
        });
        interval(10000).pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
            this.store.dispatch(new FetchCoordinates(this.id));
        });
    }

    public ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public createNotify(): void {
        const dialogRef: MatDialogRef<MapsComponent> = this.dialog.open(MapsComponent, {
            width: '1000px',
            height: '1000px',
            disableClose: true,
            data: this.coordinates
        } as MatDialogConfig);
        dialogRef.afterClosed().pipe(
            filter((notification: VesselNotification) => !!notification),
            flatMap((notification: VesselNotification) => this.notificationService.registerNotificationTask(this.id, notification))
        ).subscribe(() => this.alertService.show({
            component: RegisterTaskComponent,
            type: AlertType.INFO
        }));
    }

    public initMap(coordinates: VesselCoordinates): void {
        const center: google.maps.LatLng = new google.maps.LatLng(coordinates.latitude, coordinates.longitude);
        const zoom = 5;
        const map = new google.maps.Map(
            this.mapElement.nativeElement,
            {zoom, center}
        );
        const marker = new google.maps.Marker({
            position: center,
            draggable: true,
            map
        });
        marker.setDraggable(false);
    }
}
