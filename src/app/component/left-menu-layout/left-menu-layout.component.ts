import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

import { Observable } from 'rxjs';

import { Select, Store } from '@ngxs/store';

import { VesselModel } from '../../model/vessel.model';
import { WindowService } from '../../service/window.service';
import { VesselState } from '../../store/vessel.state';
import { InitFavoriteVessels } from '../../store/vessel.action';
import { Router } from '@angular/router';

@Component({
    selector: 'ais-left-menu-layout',
    templateUrl: './left-menu-layout.component.html',
    styleUrls: [ './left-menu-layout.component.styl' ]
})
export class LeftMenuLayoutComponent implements OnInit {
    @ViewChild(MatSidenav, {static: false}) public leftMenuComponent: MatSidenav;

    @Select(VesselState.getFavorites) public vessels$: Observable<VesselModel[]>;

    constructor(
        private readonly windowService: WindowService,
        private readonly store: Store,
        private readonly router: Router
    ) {}

    public ngOnInit(): void {
        this.store.dispatch(new InitFavoriteVessels());
    }

    public openVessel(id: number): void {
        this.router.navigate([ `/vessel/${id}` ]);
    }

    public isScreenSmall(): boolean {
        return this.windowService.smallScreen;
    }

    public toggle(): void {
        this.leftMenuComponent.toggle();
    }
}
