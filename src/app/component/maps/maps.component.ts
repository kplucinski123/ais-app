import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Loader, LoaderOptions } from '@googlemaps/js-api-loader';

import { environment } from '../../../environments/environment';
import { VesselNotification, VesselCoordinates } from '../../model/vessel.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'ais-maps',
    styleUrls: [ 'maps.component.styl' ],
    templateUrl: 'maps.component.html'
})
export class MapsComponent implements OnInit {
    @ViewChild('map') private mapElement: ElementRef;

    public form: FormGroup;

    private map: google.maps.Map;

    constructor(
        @Inject(MAT_DIALOG_DATA) private readonly data: VesselCoordinates,
        private readonly dialogRef: MatDialogRef<MapsComponent>
    ) {}

    public ngOnInit(): void {
        this.form = new FormGroup({
            minutes: new FormControl('', [ Validators.required, Validators.min(60), Validators.max(1200) ])
        });
        const options: LoaderOptions = {apiKey: environment.mapApiKey};
        new Loader(options).load().then(() => this.initMap());
    }

    public hasError(error: string): boolean {
        return this.form.controls.minutes.hasError(error);
    }

    public submit(): void {
        if (this.form.valid) {
            const notification: VesselNotification = {
                maxLatitude: this.map.getBounds().getNorthEast().lat(),
                minLatitude: this.map.getBounds().getSouthWest().lat(),
                maxLongitude: this.map.getBounds().getNorthEast().lng(),
                minLongitude: this.map.getBounds().getSouthWest().lng(),
                deadLineInMinutes: this.form.get('minutes').value
            };
            this.dialogRef.close(notification);
        } else {
            this.form.get('minutes').markAsTouched({onlySelf: true});
        }
    }

    public cancel(): void {
        this.dialogRef.close();
    }

    public initMap(): void {
        const center: google.maps.LatLng = new google.maps.LatLng(this.data.latitude, this.data.longitude);
        const zoom = 12;
        this.map = new google.maps.Map(
            this.mapElement.nativeElement,
            {zoom, center}
        );
    }
}
