import { Component, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'ais-top-bar',
    templateUrl: 'top-bar.component.html',
    styleUrls: [ 'top-bar.component.styl' ]
})
export class TopBarComponent {
    @Output() toggleEmitter: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        private readonly router: Router
    ) {}

    public menuToggle(): void {
        this.toggleEmitter.emit();
    }

    public route(component: string) {
        switch (component) {
            case 'main':
                this.router.navigate([ '/main' ]);
                break;
        }
    }
}
