import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { VesselModel } from '../../model/vessel.model';
import { VesselState } from '../../store/vessel.state';
import { AddFavoriteVessel, ApplyFilterVessels, InitVessels, RemoveFavoriteVessel } from '../../store/vessel.action';
import { Router } from '@angular/router';

@Component({
    selector: 'ais-main',
    templateUrl: 'main.component.html',
    styleUrls: [ 'main.component.styl' ]
})
export class MainComponent implements OnInit {
    public readonly displayedColumns: string[] = [ 'favorite', 'mmsi', 'name', 'shipType', 'country' ];
    public readonly pageSize: number = 20;
    public readonly defaultSort: Sort = {
        active: 'mmsi',
        direction: 'asc'
    };

    @Select(VesselState.getFiltered) public vessels$: Observable<VesselModel[]>;
    @Select(VesselState.getLength) public resultsLength$: Observable<number>;
    @Select(VesselState.getLoaded) public loaded$: Observable<boolean>;

    constructor(
        private readonly store: Store,
        private readonly router: Router
    ) {}

    public ngOnInit(): void {
        this.store.dispatch(new InitVessels({
            sort: this.defaultSort,
            pageIndex: 0,
            pageSize: this.pageSize
        }));
    }

    public openVessel(id: number): void {
        this.router.navigate([ `/vessel/${id}` ]);
    }

    public removeFavorite(id: number): void {
        this.store.dispatch(new RemoveFavoriteVessel(id));
    }

    public addFavorite(id: number): void {
        this.store.dispatch(new AddFavoriteVessel(id));
    }

    public changePage(page: PageEvent): void {
        this.store.dispatch(new ApplyFilterVessels({
            pageIndex: page.pageIndex,
            pageSize: page.pageSize
        }));
    }

    public changeSort(sort: Sort): void {
        this.store.dispatch(new ApplyFilterVessels({sort}));
    }

    public setFilter(name: string, mmsi: string): void {
        this.store.dispatch(new ApplyFilterVessels({name, mmsi}));
    }
}
