# AIS App

## O aplikacji

Aplikacja służy do monitorowania jednostek morskich oraz ustawienia powiadomienia mailowego gdy jednosta wpłynie w wybrany przez nas obszar.
Aplikacja podzielona jest na repozytorium frontend i backend.

##### Repozytorium backendowe dla aplikacji pod adresem:
```
https://gitlab.com/kplucinski123/ais-api
```

##### Uruchomienie:
Należy skopiować repozytorium (```https://gitlab.com/kplucinski123/ais-app```).
Następnie z katalogu głównego wykonać polecenie:
```docker-compose up```

## Działanie aplikacji

###Aplikacja wymaga utworzenie nowego użytkownika
![registration](screens/registration_page.png)
###Oraz zalogowanie do aplikacji
![login](screens/login_page.png)
###W przypadku problemów z logowaniem informuje nas o tym snackbar
![alert](screens/alert.png)
###Na pierwszej stronie pokazujemy użytkownikowi wszystkie dostępne statki wczytane z backend. Użytkownik ma możliwość przypiąć i odpiąć statek z lewego menu za pomocą gwiazdki na stronie głównej.
![main](screens/main_page.png)
###Po naciśnięciu na rekord w lewym menu lub na stronie głównej użytkownik zostaje przeniesiony do szczegółów statku, gdzie co 10 sekund jest odświeżana pozycja statku wyświetlana na mapie.
![main](screens/details_page.png)
###Użytkownik ma możliwość ustawienia monitorowania wpłynięcia wybranej jednostki w zaznaczony obszar.
![main](screens/modal.png)
###Po ustawieniu monitorowania jest poinformowany snackbarem.
![main](screens/alert_info.png)

##### Wykorzystane biblioteki:
- Angular Material - warstwa wizualizacyjna
- NgXS - zarządzanie stanem aplikacji
- Google Maps - wyświetlenie mapy
