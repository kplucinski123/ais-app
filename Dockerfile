FROM node:10.16 as build

WORKDIR /app

COPY package*.json /app/

RUN npm install

COPY . .

RUN npm run-script build

FROM nginx:alpine

COPY --from=build /app/dist/ais-app /usr/share/nginx/html
COPY /nginx.conf /etc/nginx/conf.d/default.conf
